describe("Editor", function () {

    /***************************************
     *              CONSTRUCTOR
     ****************************************/

    it("should be a master if specified as such when created", function () {
        var editor = new Editor(true);

        expect(editor.isMaster).toBe(true);
    });

    it("should have clientVersion as 0 when created", function () {
        var editor = new Editor(true);

        expect(editor.shadow.clientVersion).toBe(0);
    });

    it("should have masterVersion as 0 when created", function () {
        var editor = new Editor(true);

        expect(editor.shadow.masterVersion).toBe(0);
    });

    it("should be a client if specified as such when created", function () {
        var editor = new Editor(false);

        expect(editor.isMaster).toBe(false);
    });

    it("should have shadow text as an empty string when created", function () {
        var editor = new Editor(true);

        expect(editor.shadow.text).toBe("");
    });

    /***************************************
     *              DOC SYNC'
     ****************************************/

    it("should reinitialize client version to 0 when syncin'", function () {
        var editor = new Editor(false);
        editor.masterVersion = 7;
        editor.clientVersion = 7;

        editor.shadow.text = "shadow";
        editor.text = "text";

        editor.receiveSync("test");

        expect(editor.shadow.clientVersion).toBe(0);
    });

    it("should reinitialize master version to 0 when syncin'", function () {
        var editor = new Editor(false);
        editor.masterVersion = 7;
        editor.clientVersion = 7;

        editor.shadow.text = "shadow";
        editor.text = "text";

        editor.receiveSync("test");

        expect(editor.shadow.masterVersion).toBe(0);
    });

    /***************************************
    *              PREPARE UPDATE
    ****************************************/    

    it("should increment client version when prepareUpdate'", function () {
        var editor = new Editor(false);
        editor.shadow.clientVersion = 7;
        editor.prepareUpdate("test");
        expect(editor.shadow.clientVersion).toBe(8);

    });

    it("should compute the text/shadow diff patch", function () {
        var editor = new Editor(false);
        editor.shadow.text = "text";

        var patch = editor.prepareUpdate("text");

        expect(patch).toBe("[\"[]\"]");
    });

    /***************************************
    *              RECEIVE UPDATE
    ****************************************/    


    it("should increment shadow master version when receiving update as Client", function () {
        var editor = new Editor(false);

        editor.shadow.masterVersion = 7;
        editor.shadow.clientVersion = 9;
        var patch = new diff_match_patch().patch_make("test", "test");
        editor.receiveUpdate(patch, 9, 8);

        expect(editor.shadow.masterVersion).toBe(8);
    });

    it("should apply received patch to shadow on receiving update", function () {
        var editor = new Editor(true);

        editor.shadow.masterVersion = 7;
        editor.shadow.text = "test";
        editor.shadow.clientVersion = 9;

        var patch = JSON.stringify(new diff_match_patch().patch_make("test", "text"));
        patch = patch.replace('\\', '\\\\');
        patch = patch.replace("'", "{{39}}");
        
        editor.receiveUpdate([patch], 10, 7);

        expect(editor.shadow.text).toBe("text");
    });
});