//var Editor = require("../addon/narsx/editor.js").Editor;

describe("MasterEditor", function() {

	/***************************************
    *              CONSTRUCTOR
    ****************************************/

    it("should have backupVersion as 0 when created", function() {
        var editor = new Editor(true);

        expect(editor.backup.masterVersion).toBe(0);
    });

    it("should have backup text as an empty string when created", function() {
    	var editor = new Editor(true);

    	expect(editor.backup.text).toBe("");
    });

    /***************************************
    *              DOC SYNC'
    ****************************************/

	it("should reinitialize backup version to 0 when syncin'", function() {
        var editor = new Editor(true);
        editor.versionMaster = 7;
        editor.versionSlave = 7;
        editor.backupVersion = 5;

        editor.prepareSync("syncMe");

        expect(editor.backup.masterVersion).toBe(0);
    });

    it("should replace shadow with sync'd text", function() {
        var editor = new Editor(true);
        editor.versionMaster = 7;
        editor.versionSlave = 7;
        editor.backupVersion = 5;

        editor.prepareSync("syncMe");

        expect(editor.shadow.text).toBe("syncMe");
    });

    it("should replace backup with sync'd text", function() {
        var editor = new Editor(true);
        editor.versionMaster = 7;
        editor.versionSlave = 7;
        editor.backupVersion = 5;

        editor.prepareSync("syncMe");

        expect(editor.backup.text).toBe("syncMe");
    });

    /***************************************
    *              PREPARE UPDATE
    ****************************************/

    it("should increment master version when preparing update as Master", function () {
        var editor = new Editor(true);
        editor.shadow.masterVersion = 7;
        editor.prepareUpdate("test");
        expect(editor.shadow.masterVersion).toBe(8);
    });

    /***************************************
    *              RECEIVE UPDATE
    ****************************************/

    it("should increment shadow client version when receiving update as Master", function () {
        var editor = new Editor(true);

        editor.shadow.masterVersion = 9;
        editor.shadow.clientVersion = 9;

        var patch = JSON.stringify(new diff_match_patch().patch_make("test", "test"));
        patch = patch.replace('\\', '\\\\');
        patch = patch.replace("'", "{{39}}");

        editor.receiveUpdate([patch], 10, 9);

        expect(editor.shadow.clientVersion).toBe(10);
    });

    it("should increment shadow client version when receiving update as Master", function () {
        var editor = new Editor(true);

        editor.shadow.masterVersion = 9;
        editor.shadow.clientVersion = 9;

        var patch = JSON.stringify(new diff_match_patch().patch_make("test", "test"));
        patch = patch.replace('\\', '\\\\');
        patch = patch.replace("'", "{{39}}");

        editor.receiveUpdate([patch], 10, 9);

        expect(editor.shadow.clientVersion).toBe(10);
    });

    it("should reinitialize the backup text to shadow text when receiving update", function () {
        var editor = new Editor(true);

        editor.shadow.masterVersion = 7;
        editor.shadow.clientVersion = 9;
        editor.shadow.text = "text";

        var patch = JSON.stringify(new diff_match_patch().patch_make("test", "test"));
        patch = patch.replace('\\', '\\\\');
        patch = patch.replace("'", "{{39}}");

        editor.receiveUpdate([patch], 10, 7);

        expect(editor.backup.text).toBe("text");
    });

    it("should reinitialize the backup masterVersion to shadow master version when receiving update", function () {
        var editor = new Editor(true);

        editor.shadow.masterVersion = 7;
        editor.shadow.clientVersion = 9;

        var patch = JSON.stringify(new diff_match_patch().patch_make("test", "test"));
        patch = patch.replace('\\', '\\\\');
        patch = patch.replace("'", "{{39}}");

        editor.receiveUpdate([patch], 10, 7);

        expect(editor.backup.masterVersion).toBe(7);
    });

    it("should apply received patch to shadow on receiviong update", function () {
        var editor = new Editor(true);

        editor.shadow.masterVersion = 7;
        editor.shadow.text = "test";
        editor.shadow.clientVersion = 9;

        var patch = JSON.stringify(new diff_match_patch().patch_make("test", "text"));
        patch = patch.replace('\\', '\\\\');
        patch = patch.replace("'", "{{39}}");
        
        editor.receiveUpdate([patch], 10, 7);

        expect(editor.shadow.text).toBe("text");
    });
});