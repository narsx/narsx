// CodeMirror, copyright (c) by Narsx-Team
// Distributed under an MIT license: http://codemirror.net/LICENSE

// Extreme Programming done right.
//
// Adds the possibility to pair two people and let them code on the same editor.
// All the data are transmited by peer to peer.
// If you do not have your own PeerJS server, the port 9000 must be opened in order to pair peoples.
// See webRTC for information on mobile do

(function(mod) {
  if (typeof exports == "object" && typeof module == "object") // CommonJS
    mod(require("../../lib/codemirror"));
  else if (typeof define == "function" && define.amd) // AMD
    define(["../../lib/codemirror"], mod);
  else // Plain browser env
    mod(CodeMirror);
})(function(CodeMirror) {
"use strict";


    /**
     *      CHANGE THIS IN YOUR PROD ENVIRONMENT
     *
     *
     * The function called when the Master editor retrieves a PeerJS Token.
     * Use this function to register the token in a database in order to share it
     * with other clients, or make the exchange in any way you want.
     * 
     * The function is called with 3 arguments: 
     * - the token
     * - a fonction to call if registration is successful
     * - a fonction to call if registration fails
     */
    var registerTokenFunction = function(token, successCB, errorCB) {
        console.log("TOKEN= " + token);
        successCB();
    }

    /**
     * Compute the new position of the cursor after patching the edited text.
     * @param oldCursor that was created with getCursorLocation method.
     */
    CodeMirror.prototype.repositionCursor = function(oldCursor) {
        var code = this.getValue().split('\n');
        var newLoc = new diff_match_patch().match_main(this.getValue(), oldCursor.str, oldCursor.loc);
        var newCursor = {line: 0, ch: oldCursor.cursor.ch};

        var newLine = 0;
        var nbChar = 0;

        if (oldCursor.emptyString){
            newLoc += oldCursor.str.length + 1;
        }

        for (var i = 0; i < code.length; i++){
            nbChar += code[i].length;
            nbChar++;

            if (nbChar > newLoc){
                newCursor.line = i;
                break;
            }
        }

        if (oldCursor.emptyLine && this.getLine(newCursor.line) != ''){
            newCursor.line++;
        } else if (oldCursor.emptyLine && this.getLine(newCursor.line - 1) == '') {
            newCursor.line++;
        }
        this.setCursor(newCursor);
    };

    /**
     * Get all useful information in order to recalculate the cursor after an update.
     * Must be called before the editor is patch.
     * @returns an Object containing the cursor, the current string, the location of a plain text, and a boolean telling if the line is empty.
     */
    CodeMirror.prototype.getCursorLocation = function(){
        var currentPos = this.getCursor();
        var currentString = this.getLine(currentPos.line);
        var emptyLine = false;

        if (currentString == '' && currentPos.line > 0) {
            emptyLine = true;
        }

        var obj = {cursor: currentPos, str: currentString, loc: 0, emptyLine: emptyLine};

        var code = this.getValue().split('\n');
        for (var i = 0; i < currentPos.line; i++){
            if (code[i].length == 0){
                obj.loc += 1;
            } else {
                obj.loc += code[i].length + 1;    
            }
        }

        return obj;
    }

    /**
     * Apply a patch to the edited text and reposition the cursor.
     * @param patch the patch to apply
     */
    CodeMirror.prototype.patch = function(patch, shadowText) {
        var currentCursor = this.getCursorLocation();
        this.setValue(new diff_match_patch().patch_apply(patch, this.getValue())[0]);
        this.repositionCursor(currentCursor, shadowText);
    };

    /**
     * Initialize a session as Master (author).
     *
     * Instanciate the CodeMirror editor, the Master Editor object (business logic)
     * and the ConnectionHandler.
     */
    CodeMirror.defineExtension("initNarsxMasterSession", function() {
        var editor = new Editor(true);
        var cm = window.editor;

        var ch = new MasterConnectionHandler(
            registerTokenFunction,
            function(handler, data) { // onSync
                var text = cm.getValue();
                editor.prepareSync(text);
                handler.sendSyncAnswer(cm.doc.id, text);
            },
            function(handler, data) { // onUpdate
                var patch = JSON.parse(data.patch);
                editor.receiveUpdate(patch, data.clientVersion, data.masterVersion);
                if (patch.length != 0){
                    for (var i = 0; i < patch.length; i ++){
                        var p = patch[i];
                        p = p.replace("{{39}}", "'");
                        p = p.replace("\\\\", "\\");
                        cm.patch(JSON.parse(p), editor.shadow.text);
                    }
                }
                handler.resetUpdateTimeout(
                    cm.doc.id,
                    editor.prepareUpdate(cm.getValue()),
                    editor.shadow.masterVersion,
                    editor.shadow.clientVersion
                );
            }
        );
    });

    /**
     * Initialize a session as Client (collaborator).
     *
     * Instanciate the CodeMirror editor, the Client Editor object (business logic)
     * and the ConnectionHandler.
     */
    CodeMirror.defineExtension("initNarsxClientSession", function(authorToken) {
        var editor = new Editor(false);
        var cm = window.editor;

        var ch = new ClientConnectionHandler(
            authorToken,
            function(handler, data) { //onRsync
                editor.receiveSync(data.text);
                cm.setValue(data.text)
                this.resetUpdateTimeout(
                    cm.doc.id,
                    editor.prepareUpdate(cm.getValue()),
                    editor.shadow.masterVersion,
                    editor.shadow.clientVersion
                );
            },
            function(handler, data) { //onUpdate
                var patch = data.patch.replace("{{39}}", "'");
                patch = patch.replace("\\\\", "\\");
                editor.receiveUpdate(JSON.parse(patch), data.clientVersion, data.masterVersion);
                if (JSON.parse(patch).length != 0){
                    cm.patch(JSON.parse(patch), editor.shadow.text);    
                }
                this.resetUpdateTimeout(
                    cm.doc.id,
                    editor.prepareUpdate(cm.getValue()),
                    editor.shadow.masterVersion,
                    editor.shadow.clientVersion
                );
            }
        );
    });
});